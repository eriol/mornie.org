+++
date = "2006-12-19T00:33:59+01:00"
title = "Python in a Nutshell"
+++

Today, when I arrived at home I saw a package on the table in the living room:

{{< figure src="/media/python_in_a_nutshell_2edition.jpg" alt="Python in a Nutshell" >}}

My first purchase on Amazon! :)
