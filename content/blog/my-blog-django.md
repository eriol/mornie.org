---
date: 2007-03-08T05:10:33+01:00
title: My blog with Django!
---

At the end I made it!

[Django](http://www.djangoproject.com/) is great! You can build your own blog
in few minutes! :)

Many thanks to Sal Zeta for his help with this site's design!
