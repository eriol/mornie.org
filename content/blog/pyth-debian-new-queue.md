---
date: 2010-10-21T13:24:38+01:00
title: pyth in Debian NEW Queue
tags:
- debian
- python
---

Thanks to [Piotr Ożarowski](http://www.ozarowski.pl/) my first Debian package
is in the [NEW Queue](http://ftp-master.debian.org/new/pyth_0.5.6-1.html)! :)

**Update 2010-11-08:** [It's in](https://packages.qa.debian.org/p/pyth/news/20101108T220249Z.html)! :)
