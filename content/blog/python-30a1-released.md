---
date: 2007-09-01T00:12:03+01:00
title: Python 3.0a1 Released!
tags:
- python
---

Yes, the first alpha release is [out](http://python.org/download/releases/3.0/)! :D

Congratulations to all python developers for this release!!!
