---
date: 2010-12-01T07:39:00+01:00
title: django-c5filemanager
project_description: Django connector for Core Five Filemanager
---

**django-c5filemanager** (available under the BSD license) is a Django connector
for Core Five Labs file manager. It provides an easy way to add Core Five Labs
file manager to your Django project.

For more information [browse the online documentation][docs], you might be
interested in [Getting Started][docs_start] section.

The django-c5filemanager's mercurial repository is located at
<http://hg.mornie.org/django/c5filemanager>.

Feel free to mail me to report bugs and request features.

## Download ##

 * version 0.2 (14 December 2012): from [pypi][pypi_v0.2], [tar.gz][tarball_v0.2]
 * version 0.1 (01 December 2010): [tar.gz][tarball_v0.1]


[docs]: http://docs.mornie.org/django-c5filemanager/
[docs_start]: http://docs.mornie.org/django-c5filemanager/getting_started.html

[pypi_v0.2]: https://pypi.python.org/pypi/django-c5filemanager/0.2

[tarball_v0.1]: http://downloads.mornie.org/django-c5filemanager/django-c5filemanager-0.1.tar.gz
[tarball_v0.2]: http://downloads.mornie.org/django-c5filemanager/django-c5filemanager-0.2.tar.gz
