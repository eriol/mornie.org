---
date: 2012-11-01T20:42:00+01:00
title: pypel
project_description: Simple tool to manage receipts
---

**pypel** (available under the BSD license) is a is a simple tool to manage
receipts. It uses XMP to store metatada inside receipts so you can share them
in a very easy way.

For more information browse the documentation on [pypel.rtfd.org][docs].

The pypel's git repository is located at <https://github.com/eriol/pypel>.

Feel free to mail me to report bugs and request features or use
<https://github.com/eriol/pypel/issues>.

## Download ##

 * version 0.3: from [pypi][pypi_v0.3]
 * version 0.2: from [pypi][pypi_v0.2], [tar.gz][tarball_v0.2]
 * version 0.1: from [pypi][pypi_v0.1], [tar.gz][tarball_v0.1]


[docs]: http://pypel.rtfd.org

[pypi_v0.1]: https://pypi.python.org/pypi/pypel/0.1
[pypi_v0.2]: https://pypi.python.org/pypi/pypel/0.2
[pypi_v0.3]: https://pypi.python.org/pypi/pypel/0.3

[tarball_v0.1]: http://downloads.mornie.org/pypel/pypel-0.1.tar.gz
[tarball_v0.2]: http://downloads.mornie.org/pypel/pypel-0.2.tar.gz
