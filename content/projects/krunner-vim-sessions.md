---
date: 2014-02-07T02:20:00+01:00
title: krunner-vim-sessions
project_description: KRunner plugin to handle Vim sessions
---

**krunner-vim-sessions** (available under the LGPL3 license) is a
[KRunner](http://userbase.kde.org/Plasma/Krunner) plugin to handle Vim sessions.

{{< figure src="/media/krunner-vim-sessions.png" alt="krunner-vim-sessions" >}}

For more information [browse the online documentation][docs].

The krunner-vim-sessions' git repository is located at
<https://github.com/eriol/krunner-vim-sessions>.

## Download ##

 * version 0.1: [tar.gz][tarball_v0.1]


[docs]: http://docs.mornie.org/krunner-vim-sessions/

[tarball_v0.1]: http://downloads.mornie.org/krunner-vim-sessions/krunner-vim-sessions-0.1.tar.gz
