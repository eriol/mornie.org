---
date: 2011-06-05T10:58:00+01:00
title: liboceanography
project_description: C library for calculation of physical properties of sea water
---

**liboceanography** is a C library (released under LGPLv3) that provides basic
functions for calculation of physical properties of sea water.

For more information [browse the online documentation][docs].

The liboceanography's git repository is located at
<https://github.com/eriol/liboceanography>.

Feel free to mail me to report bugs and request features.
Download

 * version 1.0.0: [tar.gz][tarball_v1.0.0]


[docs]: http://docs.mornie.org/liboceanography/

[tarball_v1.0.0]: http://downloads.mornie.org/liboceanography/liboceanography-1.0.0.tar.gz
